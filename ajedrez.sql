create database Ajedrez;
use Ajedrez;
show tables;
#regionHotel
create table Hotel(
idHotel int primary key auto_increment,
Nombre varchar(40), 
Direccion varchar(40),
Telefono varchar(10)
);
drop table Hotel;

/*BUSCAR*/
drop procedure if exists p_buscar;

create procedure p_buscar(in _idHotel int,
in _Nombre varchar(40))
begin 
select * from Hotel where idHotel like _idHotel and Nombre like _Nombre;
end;

/*INGRESAR*/
drop procedure if exists p_ingresar;

create procedure p_ingresar(in _idHotel int,
in _Nombre varchar(50), 
in _Direccion varchar(50), 
in _Telefono varchar(50))
begin 
insert into Hotel values(_idHotel, _Nombre ,_Direccion, _Telefono); 
end;

/*ELIMINAR*/
drop procedure if exists p_eliminar; 

create procedure p_eliminar(in _idHotel int)
begin

delete from Hotel where idHotel = _idHotel;
end;

/*ACTUALIZAR*/
drop procedure if exists p_actualizar;

create procedure p_actualizar(in _idHotel int, in _Nombre varchar(40), in _Direccion varchar(40), in _Telefono varchar(10))
begin

declare x int;
select COUNT(*) from Hotel where idHotel = _idHotel into x;
if x <= 0 then

update Hotel set Nombre = _Nombre, Direccion = _Direccion, Telefono = _Telefono where idHotel = _idHotel;
else
update Hotel set Nombre = _Nombre, Direccion= _Direccion, Telefono = _Telefono where idHotel  = _idHotel;
end if;
end;


call p_ingresar (null,'Cuesta Real','Av.División del Norte 45','7425896');
call p_ingresar(null, 'Santa María','Boulevar Orozco y Jimenez 62','7436589');


call p_buscar(1,'Cuesta Real');

call p_eliminar(2);

call p_actualizar(1,'Hotel Senda','Avenida #2','578962');

select * from hotel;
#end region#

#regionSala 
create table Sala(
idsala int primary key auto_increment, 
Codigo_Sala varchar(40),
medio varchar(40),
Capacidad int,
fkidHotel int, 
FOREIGN KEY (FkidHotel) REFERENCES Hotel (idHotel)
);
drop table Sala;
/* tabla de Sala*/
drop procedure if exists p_buscar;

create procedure p_buscar(in _idsala int,
in _Codigo_sala int)
begin 
select * from Sala where idSala like _idSala and Codigo_sala like _Codigo_sala;
end;

/*INGRESAR*/
drop procedure if exists p_ingresar;

create procedure p_ingresar(in _idSala int,
in _Codigo_sala int, 
in _medio varchar(50), 
in _capacidad varchar(50),
in _fkidHotel int)
begin 
insert into Sala values(_idSala, _Codigo_Sala ,_medio, _capacidad, _fkidHotel);
end;
describe sala;

/*ELIMINAR*/
drop procedure if exists p_eliminar; 

create procedure p_eliminar(in _idSala int)
begin
delete from Sala where idSala = _idSala;
end;

/*ACTUALIZAR*/
drop procedure if exists p_actualizar;

create procedure p_actualizar(in _idSala int, in _Codigo_sala int , in _medio varchar(50), in _capacidad varchar(50), in _fkidHotel int)
begin

declare x int;
select COUNT(*) from Sala where idSala = _idSala into x;
if x <= 0 then

update Sala set Codigo_sala = _codigo_sala, medio = _medio, capacidad= _capacidad, fkidHotel = _fkidHotel where idSala = _idSala;
else
update Sala set Codigo_sala = _Codigo_sala, medio = _medio, capacidad = _capacidad, fkidHotel = _fkidHotel where idSala = _idSala;
end if;
end;
describe sala;

call p_ingresar (null,'Sala',' NULL','15','1');
call p_ingresar (null,'AHTFNVNE','NULL','12','');
call p_buscar(null,'ASDFGHEW');

call p_eliminar(1);
select * from sala;
call p_actualizar (3,'ACOLEGIO',' NULL','15','1');
#end region;

#regionPais
create table Pais(
idPais int primary key auto_increment,
Pais varchar (60),
Nombre varchar(40),
N_clubs int);

drop procedure if exists p_buscarPais;

create procedure p_buscarPais(in _idPais int,
in _Pais varchar(60))
begin 
select * from Pais where idPais like _idPais and Pais like _Pais;
end;

drop procedure if exists p_ingresarpais;
create procedure p_ingresarPais(in _idPais int,
in _Pais varchar(60), 
in _Nombre varchar(40), 
in _N_clubs int)
begin 
insert into Pais values(_idPais, _Pais ,_Nombre, _N_clubs); 
end;

/*ELIMINAR*/
drop procedure if exists p_eliminarPais; 

create procedure p_eliminarPais(in _idPais int)
begin

delete from Pais where idPais = _idPais;
end;

/*ACTUALIZAR*/
drop procedure if exists p_actualizarPais;

create procedure p_actualizarPais (in _idPais int, in _Pais varchar(60), in _Nombre varchar(40), in _N_clubs int )
begin

declare x int;
select COUNT(*) from Pais where idPais = _idPais into x;
if x <= 0 then

update Pais set Pais = _Pais, Nombre = _Nombre, N_clubs = _N_clubs where idPais = _idPais;
else
update Pais set Pais = _Pais, Nombre= _Nombre, N_clubs = _N_clubs where idPais  = _idPais;
end if;
end;

call p_ingresarPais (1,'Mexico',' Guadalajara','3');
call p_ingresarPais (2,'Mexico','Jalisco','11');
call p_buscarPais(2,'Mexico');

call p_eliminarPais(1);
select * from Pais;
call p_actualizar ('Mexico','Jalisco','13');
describe pais;
show tables;
#end region pais

#region participante
create table Participante(
idParticipante int primary key auto_increment,
N_social int, 
Nombre varchar(40),
direcion varchar(40),
NombreCampeonato varchar(40),
tipoCampeonato varchar(40),
telefono int,
fkidPais int,
FOREIGN KEY (fkidPais) REFERENCES Pais (idPais)
);
drop table participante;


drop procedure if exists p_buscarParticipante;

create procedure p_buscarParticipante (in _idparticipante int,
in _N_social int)
begin 
select * from Participante where idParticipante like _idParticipante and N_social like _N_social;
end;

drop procedure if exists p_ingresarparticipante;

create procedure p_ingresarParticipante(in _idParticipante int,
in _N_social int , 
in _Nombre varchar(40), 
in _direccion varchar(40),
in _NombreCampeonato varchar(40),
in _tipoCampeonato varchar(40),
in _telefono varchar(40),
in _fkispais int)
begin 
insert into Pais values(_idParticipante, _N_social ,_direccion, _NombreCampeonato, _tipoCampeonato, _telefono, _fkidpais); 
end;
describe participante;
/*ELIMINAR*/
drop procedure if exists p_eliminarParticipante; 

create procedure p_eliminarParticipante(in _idParticipante int)
begin
delete from Participante where idParticipante = _idParticipante;
end;

/*ACTUALIZAR*/
drop procedure if exists p_actualizarParticipante;

create procedure p_actualizarParticipante (in _idParticipante int, in _N_social int, in _Nombre varchar(40), in direccion varchar(40),
in NombreCampeonato varchar(40),tipoCampeonato varchar(40), telefono int)
begin
declare x int;
select COUNT(*) from Participante where idParticipante = _idParticipante into x;
if x <= 0 then

update Participante set N_social = _N_social, Nombre = _Nombre, direccion = direccion, NombreCampeonato =_NombreCampeonato, 
tipocampeonato =_tipocampeonato, telefono =_telefono, fkidpais = _fkidpais
where idParticipante = _idParticipante;
else
update Participante set N_social =  N_social , Nombre = _Nombre, direccion = direccion, NombreCampeonato =_NombreCampeonato, 
tipocampeonato =_tipocampeonato, telefono =_telefono, fkidpais = _fkidpais;
end if;
end;
#end region participante

#region Aloja
create table Aloja(
idAloja int primary key auto_increment,
fecha_entrada date,
fecha_salida date,
fkidParticipante int ,
fkidhotel int,
FOREIGN KEY (fkidParticipante) REFERENCES Participante (idParticipante),
FOREIGN KEY (fkidHotel) REFERENCES Hotel (idHotel));

drop procedure if exists p_buscarAloja;

create procedure p_buscarAloja (in _idAloja int,
in _fecha_entrada int)
begin 
select * from Aloja where idAloja like _idAloja and fecha_entrada like _fecha_entrada;
end;

drop procedure if exists p_ingresarAloja;
create procedure p_ingresarAloja(in _idAoja int,
in _fecha_entrada date, 
in _fecha_salida date,
in _fkidparticipante int,
in _fkidhotel int)
begin 
insert into Pais values(_fecha_entrada, _fecha_salida,_fkidparticipante, _fkidhotel);
end;
describe Aloja;
/*ELIMINAR*/
drop procedure if exists p_eliminarAloja; 

create procedure p_eliminarAloja(in _idAloja int)
begin
delete from Aloja where idAloja = _idAloja;
end;

/*ACTUALIZAR*/
drop procedure if exists p_actualizarAloja;

create procedure p_actualizarAloja (in _idAloja int, in  _fecha_entrada date , in _fecha_salida date, in fkidparticipante int, fkidhotel int)
begin
declare x int;
select COUNT(*) from Aloja where idAloja = _idAloja into x;
if x <= 0 then

update Aloja set fecha_entrada =_fecha_entrada, fecha_salida = _fecha_salida, fkidparticipante =_fkidparticipante, fkidhotel = _fkidhotel
where idAloja = _idAloja;
else
update Aloja set fecha_entrada =  fecha_entrada ,fecha_salida = _fecha_salida, fkidparticipante =_fkidparticipante, fkidhotel = _fkidhotel
where idAloja = _idAloja;
end if;
end;
#end region Aloja


#region jugador
drop table Jugador;
select * from Jugador;
create table Jugador(
Id_Jugador int primary key  AUTO_INCREMENT,
Nivel varchar(40),
fkidParticipante int,
FOREIGN KEY (fkidParticipante) REFERENCES Participante (idParticipante)
);

drop procedure if exists p_buscarJugador;
create procedure p_buscarJugador (in _idJugador int,
in _Nivel varchar(40))
begin 
select * from Jugador where idJugador like _idJugador and Nivel like _Nivel;
end;

drop procedure if exists p_ingresarJugador;
create procedure p_ingresarJugador(in _idJugador int,
in _Nivel varchar(40), 
in _fkidparticipante int)
begin 
insert into Jugador values(_nivel, _fkidparticipante);
end;

drop procedure if exists p_eliminarJugador; 
create procedure p_eliminarJugador(in _idJugador int)
begin
delete from Jugador where idJugador = _idJugador;
end;

drop procedure if exists p_actualizarJugador;
create procedure p_actualizarJugador (in _idJugador int, in  _Nivel int , in _fkidparticipante int)
begin
declare x int;
select COUNT(*) from Jugador where idJugador = _idJugador into x;
if x <= 0 then

update Jugador set Nivel =_Nivel, fkidParticipante = _fkidParticipante
where idJugador = _idJugador;
else
update Jugador set Nivel = _Nivel, fkidparticipante = _fkidparticipante 
where idJugador = _idJugador;
end if;
end;
#end region Jugador

#region Albitro
create table Albrito(
id_Albrito int primary key  AUTO_INCREMENT,
Nombre varchar(50),
fkidParticipante int,
FOREIGN KEY (fkidParticipante) REFERENCES Participante (idParticipante)
);
drop  table Albrito;
drop procedure if exists p_buscarAlbitro;
create procedure p_buscarAlbitro(in _idAlbitro int,in Nombre varchar(50),
in _fkidParticipante int)
begin 
select * from Albitro where idAlbitro like _idAlbitro and Nombre like _Nombre;
end;


drop procedure if exists p_ingresarAlbitro;
create procedure p_ingresarAlbitro(in _idAlbitro int,
in _Nombre varchar(50), 
in _fkidparticipante int)
begin 
insert into Albitro values(_Nombre, _fkidparticipante);
end;

drop procedure if exists p_eliminarAlbitro; 
create procedure p_eliminarAlbitro(in _idAlbitro int)
begin
delete from Albitro where idAlbitro = _idAlbitro;
end;


drop procedure if exists p_actualizarAlbitro;
create procedure p_actualizarAlbitro (in _idAlbitro int, in  _Nombre varchar(50), in _fkidparticipante int)
begin
declare x int;
select COUNT(*) from Albitro where idAlbitro = _idAlbitro into x;
if x <= 0 then

update Albitro set Nombre=_Nombre, fkidParticipante = _fkidParticipante
where idAlbitro = _idAlbitro;
else
update Albitro set Nombre = _Nombre, fkidparticipante = _fkidparticipante 
where idAlbitro = _idAlbitro;
end if;
end;
#end region Albitro

create table Partida(
idPartida int primary key auto_increment,
Codigo_Partida int,
Dia int,
mes int,
año int,
fkidsala int,
FOREIGN KEY (fkidSala) REFERENCES Sala (idSala)
);
drop table Sala;
describe sala;

drop procedure if exists p_buscarPartida;
create procedure p_buscarPartida(in _idPartida int,in Codigo_Partida int, in dia int, in mes int,in año int,in fkidsala int)
begin 
select * from Partida where idPartida  like _idPartida and Codigo_Partida like _Codigo_Partida;
end;

drop procedure if exists p_ingresarPartida;
create procedure p_ingresarPartida(in _idPartida int,
in _Codigo_Partida int, 
in _Dia int,
in _mes int,
in _año int,
in _fkidPartida int)
begin 
insert into Partida values(_Codigo_Partida, _Dia, _mes,_año, _fkidpartida);
end;

create table Movimiento(
id_Movimiento int primary key  AUTO_INCREMENT,
jugada int,
Movimiento int,
Comentario varchar(100),
fk_idPartida int,
FOREIGN KEY (fk_idPartida) REFERENCES  Partida(idPartida)
);

show tables;
